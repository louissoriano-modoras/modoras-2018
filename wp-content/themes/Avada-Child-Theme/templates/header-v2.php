<?php
/**
 * Header-v2 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
			<div class="fusion-header-has-flyout-menu-content">
		<?php endif; ?>
		<?php avada_logo(); ?>

		<div class="headercontact">



			<div class="client-access">

				<div role="button" class="trigger<?php echo (modoras_is_client_area_login() ? ' open' : ''); ?>" data-is-client-area="<?php echo json_encode(modoras_is_client_area_login()); ?>">Client Access</div>

						<div class="client-access__container">
							<h4>Client Access</h4>

							<?php if (modoras_is_client_area_login()) { ?>
								<div class="login-form">

									<form action="https://www2.modoras.com/clientaccess/login.aspx" method="post">
										<input name="username2" placeholder="Username" type="text" id="login-form-username2">
										<input name="password2" placeholder="Password" type="password">
										<button type="submit" class="btn">Login</button>
									</form>

								</div><!-- /.login-form -->
							<?php } else { ?>
								<div class="client-access__options">
									<div class="client-access__options--item">
										<a href="https://cloud1.sagehandisoft.com.au/ClientPortal/Account/Login/MODO0002" class="btn" target="_blank">Accounting Client Access * <i class="fa-arrow-right fas button-icon-right"></i></a> 
									</div>
									
									<div class="client-access__options--item">
										<a href="#" class="btn" target="_blank" href="#" data-toggle="modal" data-target=".fusion-modal.clientaccess">Wealth Management Client Access <i class="fa-arrow-right fas button-icon-right"></i></a>
									</div>
								</div><!-- /.client-area__options -->
								<div class="client-access__disclaimer">
									* You are about to leave the Modoras website and be directed to Sage Handisoft - our cloud accounting software partners.
								</div>
							<?php } ?>
						</div><!-- /.client-access__container -->
					</div><!-- /.client-access -->




			<ul>
				<li><a href="https://www.facebook.com/Modoras/" target="_blank"><img src="<?php bloginfo('url');?>/wp-content/uploads/2018/04/facebook_icon.png"></a></li>
				<li><a href="https://twitter.com/modoras" target="_blank"><img src="<?php bloginfo('url');?>/wp-content/uploads/2018/04/twitter_icon.png"></a></li>
				<li><a href="https://www.linkedin.com/company/modoras" target="_blank"><img src="<?php bloginfo('url');?>/wp-content/uploads/2018/04/linkedin_icon.png"></a></li>
				<li><a href="https://www.youtube.com/channel/UCxtex3ouJcHIPwFCwhkEQlQ" target="_blank"><img src="<?php bloginfo('url');?>/wp-content/uploads/2018/04/youtube_icon.png"></a></li>
			</ul>
			<span class="ph"><img src="<?php bloginfo('url');?>/wp-content/uploads/2018/04/ph_icon.png"> <a href="tel:+1300888803">1300 888 803</a></span>
		</div>
		
		<?php avada_main_menu(); ?>
		<?php if ( 'flyout' === Avada()->settings->get( 'mobile_menu_design' ) ) : ?>
			</div>
		<?php endif; ?>
	</div>
</div>
