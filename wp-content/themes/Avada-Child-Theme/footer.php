<?php
/**
 * The footer template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
					<?php do_action( 'avada_after_main_content' ); ?>

				</div>  <!-- fusion-row -->
			</main>  <!-- #main -->
			<?php do_action( 'avada_after_main_container' ); ?>


			<div class="special-footer" style="display: none;">











			<div class="fusion-fullwidth fullwidth-box fusion-parallax-none footerwrapper nonhundred-percent-fullwidth non-hundred-percent-height-scrolling" style="background-color: #1f293e;background-image: url(&quot;<?php bloginfo('url');?>/wp-content/uploads/2018/04/footer_bg.png&quot;);background-position: center center;background-repeat: repeat;padding-top:50px;padding-right:30px;padding-left:30px;"><div class="fusion-builder-row fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style="margin-top:0px;margin-bottom:20px;">
					<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
						<div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3" style="margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );margin-right:4%;"><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url=""><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="<?php bloginfo('url');?>/wp-content/uploads/2018/04/footer_logo.png" width="276" height="59" alt="" title="footer_logo" class="img-responsive wp-image-12158" srcset="<?php bloginfo('url');?>/wp-content/uploads/2018/04/footer_logo-200x43.png 200w, <?php bloginfo('url');?>/wp-content/uploads/2018/04/footer_logo.png 276w" sizes="(max-width: 800px) 100vw, 276px"></span></div></div><div class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-last 2_3" style="margin-top: 0px;margin-bottom: 20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );"><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url=""><div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area"><style type="text/css" scoped="scoped">.fusion-widget-area-1 {padding:0px 0px 0px 0px;}.fusion-widget-area-1 .widget h4 {color:#333333;}.fusion-widget-area-1 .widget .heading h4 {color:#333333;}.fusion-widget-area-1 .widget h4 {font-size:14px;}.fusion-widget-area-1 .widget .heading h4 {font-size:14px;}</style><div id="menu-widget-2" class="widget menu"><style type="text/css">#menu-widget-2{text-align:left;}#menu-widget-2 > .fusion-widget-menu li{display: inline-block;}#menu-widget-2 ul li a{display: inline-block;padding:0;border:0;color:#ccc;font-size:14px;}#menu-widget-2 ul li a:after{content:'|';color:#ccc;padding-right:25px;padding-left:25px;font-size:14px;}#menu-widget-2 ul li a:hover, #menu-widget-2 ul .menu-item.current-menu-item a {color:#fff;}#menu-widget-2 ul li:last-child a:after{display: none}#menu-widget-2 ul li .fusion-widget-cart-number{margin:0 7px;background-color:#fff;color:#ccc;}#menu-widget-2 ul li.fusion-active-cart-icon .fusion-widget-cart-icon:after{color:#fff;}</style><nav class="fusion-widget-menu"><ul id="menu-footer-main-navigation" class="menu"><li id="menu-item-12510" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-12332 current_page_item menu-item-12510"><a href="<?php bloginfo('url');?>/why/">Why</a></li><li id="menu-item-13515" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13515"><a href="<?php bloginfo('url');?>/services/">Services</a></li><li id="menu-item-12574" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12574"><a href="<?php bloginfo('url');?>/knowledge/">Knowledge Centre</a></li><li id="menu-item-12509" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12509"><a href="<?php bloginfo('url');?>/about-us/">About Us</a></li><li id="menu-item-13350" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13350"><a href="<?php bloginfo('url');?>/news/">News</a></li><li id="menu-item-13351" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13351"><a href="<?php bloginfo('url');?>/our-events/">Events</a></li><li id="menu-item-13352" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13352"><a href="<?php bloginfo('url');?>/blogs/">Blogs</a></li><li id="menu-item-12603" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12603"><a href="<?php bloginfo('url');?>/contact/">Contact</a></li></ul></nav></div><div class="fusion-additional-widget-content"></div></div></div></div></div><div class="fusion-builder-row fusion-builder-row-inner fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first 1_4" style="margin-top: 0px;margin-bottom: 20px;width:25%;width:calc(25% - ( ( 4% ) * 0.25 ) );margin-right:4%;"><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url=""><div class="fusion-text"><p><a style="color: #f35d4d; font-size: 18px;" href="mailto:info@modoras.com">info@modoras.com</a><br>
<span style="font-size: 30px;"><a style="color: #ffffff; font-weight: bold;" href="tel:+1300888803">1300 888 803</a></span></p>
</div><div class="fusion-social-links"><div class="fusion-social-networks"><div class="fusion-social-networks-wrapper"><a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" style="color:#bebdbd;" aria-label="fusion-facebook" target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/Modoras/" data-placement="top" data-title="Facebook" data-toggle="tooltip" title="" data-original-title="Facebook"></a><a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" style="color:#bebdbd;" aria-label="fusion-twitter" target="_blank" rel="noopener noreferrer" href="https://twitter.com/modoras" data-placement="top" data-title="Twitter" data-toggle="tooltip" title="" data-original-title="Twitter"></a><a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" style="color:#bebdbd;" aria-label="fusion-linkedin" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/modoras" data-placement="top" data-title="Linkedin" data-toggle="tooltip" title="" data-original-title="Linkedin"></a><a class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube" style="color:#bebdbd;" aria-label="fusion-youtube" target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/channel/UCxtex3ouJcHIPwFCwhkEQlQ" data-placement="top" data-title="YouTube" data-toggle="tooltip" title="" data-original-title="YouTube"></a></div></div></div></div></div><div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_4  fusion-three-fourth fusion-column-last 3_4" style="margin-top: 0px;margin-bottom: 20px;width:75%;width:calc(75% - ( ( 4% ) * 0.75 ) );"><div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url=""><div class="fusion-content-boxes content-boxes columns row fusion-columns-4 fusion-columns-total-4 fusion-content-boxes-1 content-boxes-icon-with-title content-left" data-animationoffset="100%" style="margin-top:0px;margin-bottom:60px;"><div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row"><div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationoffset="100%"><div class="heading icon-left"><h2 class="content-box-heading" style="font-size:15px;line-height:20px;" data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="15" data-lineheight="20">Brisbane (Head Office)</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="color:#c1c1c1;">(07) 3219 2555<br>
Level 3, 50-56 Sanders St<br>
PO Box 6530<br>
Upper Mt Gravatt QLD 4122</div></div></div><div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover "><div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationoffset="100%"><div class="heading icon-left"><h2 class="content-box-heading" style="font-size:15px;line-height:20px;" data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="15" data-lineheight="20">Gold Coast</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="color:#c1c1c1;">(07) 5570 6150<br>
Suite 38, Level 4, 46 Cavill Ave<br>
PO Box 2322<br>
Surfers Paradise QLD 4217</div></div></div><div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover "><div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationoffset="100%"><div class="heading icon-left"><h2 class="content-box-heading" style="font-size:15px;line-height:20px;" data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="15" data-lineheight="20">Sydney</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="color:#c1c1c1;">(02) 9923 2499<br>
Level 2, Suite 201,<br>
157 Walker Street<br>
North Sydney, NSW 2060</div></div></div><div class="fusion-column content-box-column content-box-column content-box-column-4 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-last"><div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationoffset="100%"><div class="heading icon-left"><h2 class="content-box-heading" style="font-size:15px;line-height:20px;" data-inline-fontsize="true" data-inline-lineheight="true" data-fontsize="15" data-lineheight="20">Melbourne</h2></div><div class="fusion-clearfix"></div><div class="content-container" style="color:#c1c1c1;">(03) 9391 8332<br>
39 Cabot Drive<br>
PO Box 627<br>
Altona North VIC 3025</div></div></div><div class="fusion-clearfix"></div></div></div></div></div><div class="fusion-text"><p style="font-size: 11px; text-align: center;">Financial Planning and Credit services are offered through Modoras Pty Ltd ABN 86 068 034 908. Australian Financial Services and Credit Licence No. 233209.<br>
Accounting services are offered through Modoras Accounting (QLD) Pty Ltd ABN 81 601 145 215, Modoras Accounting (VIC) Pty Ltd ACN 145 368 850 and Modoras Accounting (SYD) Pty Ltd ABN 18 622 475 521</p>
</div><div class="fusion-clearfix"></div>

					</div>
				</div></div></div>









				<div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling" style="background-color: #1a2131;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;"><div class="fusion-builder-row fusion-row "><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2" style="margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;">
					<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
						<div class="fusion-text"><p>Copyright Modoras 2018. All Rights Reserved.</p>
</div><div class="fusion-clearfix"></div>

					</div>
				</div><div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2" style="margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );">
					<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
						<div class="fusion-text"><p style="text-align: right;"><a style="color: #747474; text-decoration: underline;" href="/new-website/privacy-policy/">Privacy Policy</a></p>
</div><div class="fusion-clearfix"></div>

					</div>
				</div></div></div>





</div>









			<?php global $social_icons; ?>

			<?php
			/**
			 * Get the correct page ID.
			 */
			$c_page_id = Avada()->fusion_library->get_page_id();
			?>

			<?php
			/**
			 * Only include the footer.
			 */
			?>
			<?php if ( ! is_page_template( 'blank.php' ) ) : ?>
				<?php $footer_parallax_class = ( 'footer_parallax_effect' === Avada()->settings->get( 'footer_special_effects' ) ) ? ' fusion-footer-parallax' : ''; ?>

				<div class="fusion-footer<?php echo esc_attr( $footer_parallax_class ); ?>">
					<?php get_template_part( 'templates/footer-content' ); ?>
				</div> <!-- fusion-footer -->
			<?php endif; // End is not blank page check. ?>

			<?php
			/**
			 * Add sliding bar.
			 */
			?>
			<?php if ( Avada()->settings->get( 'slidingbar_widgets' ) && ! is_page_template( 'blank.php' ) ) : ?>
				<?php get_template_part( 'sliding_bar' ); ?>
			<?php endif; ?>
		</div> <!-- wrapper -->

		<?php
		/**
		 * Check if boxed side header layout is used; if so close the #boxed-wrapper container.
		 */
		$page_bg_layout = 'default';
		if ( $c_page_id && is_numeric( $c_page_id ) ) {
			$fpo_page_bg_layout = get_post_meta( $c_page_id, 'pyre_page_bg_layout', true );
			$page_bg_layout = ( $fpo_page_bg_layout ) ? $fpo_page_bg_layout : $page_bg_layout;
		}
		?>
		<?php if ( ( ( 'Boxed' === Avada()->settings->get( 'layout' ) && 'default' === $page_bg_layout ) || 'boxed' === $page_bg_layout ) && 'Top' !== Avada()->settings->get( 'header_position' ) ) : ?>
			</div> <!-- #boxed-wrapper -->
		<?php endif; ?>
		<?php if ( ( ( 'Boxed' === Avada()->settings->get( 'layout' ) && 'default' === $page_bg_layout ) || 'boxed' === $page_bg_layout ) && 'framed' === Avada()->settings->get( 'scroll_offset' ) && 0 !== intval( Avada()->settings->get( 'margin_offset', 'top' ) ) ) : ?>
			<div class="fusion-top-frame"></div>
			<div class="fusion-bottom-frame"></div>
			<?php if ( 'None' !== Avada()->settings->get( 'boxed_modal_shadow' ) ) : ?>
				<div class="fusion-boxed-shadow"></div>
			<?php endif; ?>
		<?php endif; ?>
		<a class="fusion-one-page-text-link fusion-page-load-link"></a>

		<script type="text/javascript">


			jQuery(document).ready(function(){
   jQuery( '.client-access .trigger' ).click(function(event){
		jQuery(this).toggleClass( 'open' );

		if ( jQuery( 'html' ).is( '.ie' ) ) {
			jQuery( this ).parent().addClass( 'repaint-fix' ).removeClass( 'repaint-fix' );
		}

		if (jQuery(this).hasClass('open')) {
			jQuery('#login-form-username2').focus();
		}
	});
});




			

		</script>

		<?php wp_footer(); ?>
	</body>
</html>
