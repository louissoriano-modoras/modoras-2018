jQuery(document).ready(function($) {

	$( 'html' ).removeClass( 'no-js jquery-loading' );

	//$( '#input_1_1:first' ).attr( 'id', 'input_1_1-2' );


	/*
	============================================================
		Responsive Images in Columns
	============================================================

	This is needed to support IE & FF browsers because they
	don't support max-width:100%; for elements with unkown
	widths, i.e. the parent element doesn't have width set.

	*/

	var colParagraphs = $('.col p img');
	var col = $('.col-50');

	set_col_paragraph_widths = function() {

		if (col.css("float") == "none" ){
			imageWidth = col.width()
		} else {
			imageWidth = col.width() - 112;
		}

		colParagraphs.each(function( index ) {
			$(this).css( 'width', imageWidth );
		});

	}

	$(window).resize(function(){
		set_col_paragraph_widths();
	});

	set_col_paragraph_widths();


	/*
	============================================================
		Client Access open/close button
	============================================================
	*/

	if ($('.client-access .trigger.open').length > 0) {
		$('#login-form-username2').focus();
	}

	$( '.client-access .trigger' ).click(function(event){
		$(this).toggleClass( 'open' );

		if ( $( 'html' ).is( '.ie' ) ) {
			$( this ).parent().addClass( 'repaint-fix' ).removeClass( 'repaint-fix' );
		}

		if ($(this).hasClass('open')) {
			$('#login-form-username2').focus();
		}
	});



	/*
	============================================================
		Mobile flying menu
	============================================================
	*/

	(function(){
		var menu = $('#header-menu');

		var setMenuClass = function() {
			if (Math.floor(parseInt(menu.css('margin-top')) - window.scrollY) <= 0) {
				menu.addClass('past-top');
			}
			else {
				menu.removeClass('past-top');
			}
		};

		$(window).on('load scroll resize', function(){
			setMenuClass();
		});

	})();



	/*
	 ==============================================================
	 Mobile menu title
	 ==============================================================
	 */

	var headerMenuOverlay = $('<div class="header-menu-overlay"></div>');
	var headerMenuTitle = $('.header-menu-title');
	var headerMenuTitleClone = headerMenuTitle.clone().addClass('header-menu-title-clone');

	headerMenuTitle.addClass('header-menu-title-default');

	headerMenuTitleClone.insertAfter(headerMenuTitle);

	headerMenuOverlay.prependTo($('#header-menu'));

	headerMenuOverlay.on('click', function () {
		headerMenuTitle.trigger('click');
	});

	headerMenuTitle.add(headerMenuTitleClone).click(function(){
		$('html').toggleClass('mobile-menu-open');

		if ($('html').hasClass('mobile-menu-open')) {
			headerMenuTitle[0].scrollIntoView(true);
		}

		return false;
	});



	/*
	============================================================
		Profile bio excerpt
	============================================================
	*/
	(function(){
		$(".profile .bio").each(function(){

			var textContent = $(this).html();

			$(this).excerpt({ lines: 7 });

			$(this).after('<a href="#" class="more">more</a>');

			$(this).siblings(".more").click(function(event) {

				if (event.preventDefault()) {
					event.preventDefault();
				} else {
					event.returnValue = false;
				}

				if ($(this).text() == "more") {

					$(this).siblings(".bio").html(textContent);
					$(this).text("less");

				} else if ($(this).text() == "less") {

					$(this).siblings(".bio").excerpt({ lines: 7 });
					$(this).text("more");

				}
			});
		});
	})();


	/*
	============================================================
		Sidebar search form container focus
	============================================================
	*/
	(function(){
		$( '.sidebar .search-form .search-field' ).focus(function(){
			$( this ).parents( '.container' ).addClass( 'focus' );
		}).blur(function(){
			$( this ).parents( '.container' ).removeClass( 'focus' );
		});
	})();


	/*
	============================================================
		Line numbers
	============================================================
	*/
	(function(){
		$( '.market-insights .title' ).excerpt( { lines: 2 } );
		$( '.market-insights .excerpt' ).excerpt( { lines: 3 } );

		$( '.entry-list.rows-4 .entry-callout:not(.videos) .title' ).excerpt( { lines: 2 } );
		$( '.entry-list.rows-4 .entry-callout.videos .title' ).excerpt( { lines: 3 } );
		$( '.entry-list.rows-4 .entry-callout .excerpt' ).excerpt( { lines: 4 } );

		$( '.entry-list.rows-1 .entry-callout .title' ).excerpt( { lines: 2 } );
		$( '.entry-list.rows-1 .entry-callout .excerpt' ).excerpt( { lines: 3 } );

		$( '.case-study .quote' ).excerpt( { lines: 3 } );

		$( '.latest-posts ul a' ).excerpt( { lines: 2 } );
	})();



	/*
	============================================================
		Data Excerpt
	============================================================
	*/
	$('[data-excerpt-lines]').each(function(){
		$(this).excerpt({lines: $(this).data('excerpt-lines')});
	});



	/*
	============================================================
		Add placeholders to name fields
	============================================================
	*/
	$('.newsletter .name input, .name-field input, .form-condensed .name_first input, .form-condensed .name_last input').each(function () {
		$(this).attr('placeholder', $(this).siblings('label').text() + ' Name');
	});



	/*
	============================================================
		Add placeholders to multiple column fields
	============================================================
	*/
	$('.gform-multiple-column-list').find('.gfield_list').each(function(){
		var table = $(this);

		$(this).find('th').each(function(){
			$(table.find('tbody td').get($(this).index())).find('input').attr('placeholder', $(this).text());
		});
	});
	
	/*
	============================================================
		Disable placeholders if the field is already populated
	============================================================
	*/
	$(document).ready(function() {
		$('.gform_wrapper input:not(.gform_hidden,[type="checkbox"],[type="radio"])').each(function() {
			var input = $(this);
			var label = $(this).siblings('label');
			
			//Interrupt the execution thread to allow input to update
			setTimeout(function() {
				if ( input.val() == '' )
					label.show();
				else
					label.hide();
			}, 0);
		});
	});


	/*
	============================================================
		How to Choose an Accountant page form address labels as
		placeholders
	============================================================
	*/
	(function(){
		$('body')
			.on('keypress keydown change load', '.gform_wrapper input:not(.gform_hidden,[type="checkbox"],[type="radio"])', function() {
				var input = $(this);
				var label = $(this).siblings('label');
				//Interrupt the execution thread to allow input to update
				setTimeout(function() {
					if ( input.val() == '' )
						label.show();
					else
						label.hide();
				}, 0);
			})

			.on('focus', '.gform_wrapper input:not(.gform_hidden,[type="checkbox"],[type="radio"])', function(){
				$(this).siblings('label').addClass( 'focus' );
			})

			.on('blur', '.gform_wrapper input:not(.gform_hidden,[type="checkbox"],[type="radio"])', function(){
				$(this).siblings('label').removeClass( 'focus' );
			});
	})();


	/*
	============================================================
		Placeholders for all browsers
	============================================================
	*/
	$('[placeholder]')
		// What if the user types in the exact placeholder text?
		.each(function(){
			var input = $(this);
			var placeholder = "\u200b" + $(this).attr( 'placeholder' );

			// Add non-print character
			$(this).attr( 'placeholder', placeholder )

				// Add placeholder
				.blur(function() {
					if ( input.val() == '' || input.val() == placeholder ) {
						input.addClass( 'placeholder' ).val( placeholder );
					}
				})

				// Remove placeholder
				.focus(function() {
					if ( input.val() == placeholder ) {
						input.removeClass( 'placeholder' ).val( '' );
					}
				})

				// Init
				.blur()

				// Clear placeholder before submitting
				.parents('form')
					.submit(function() {
						$(this).find('[placeholder]').each(function() {
							var input = $(this);
							if ( input.val() == placeholder ) {
								input.val('');
							}
						})
					});
		});



	/*
	============================================================
		Homepage slider IE fallback background image
	============================================================
	*/

	if ($('html').hasClass('ie7') || $('html').hasClass('ie8')) {
		$('.hero-slider .slide').each(function () {
			var backgroundImage = $(this).css('background-image');
			backgroundImage = /^url\((['"]?)(.*)\1\)$/.exec(backgroundImage);
			backgroundImage = backgroundImage ? backgroundImage[2] : "";

			$(this).css('background-image', '');

			var image = $('<img>').attr({
				'src': backgroundImage,
				'class': 'hero__background-img-fallback'
			});

			$(this).prepend(image);
		})
	};



	/*
	============================================================
		Cycle
	============================================================
	*/
	(function(){
		$('.hero-slider .slides').each(function(){
			$(this).cycle({
				speed:           800,
				timeout:         5000,
				delay:           -1000,
				pause:           true,
				prev:            '.hero-slider .prev',
				next:            '.hero-slider .next',
				pager:           '.hero-slider .slider-nav',
				containerResize: false,
				width:           '100%',
				fit:             1
			});

			$(document.documentElement).keyup(function (e) {
				if (e.keyCode == 37) {
					$('.hero-slider .prev').click();
				}
				if (e.keyCode == 39) {
					$('.hero-slider .next').click();
				}
			});

		});

		$('.case-studies.section-block').each(function(){
			$(this)
				.append( '<div class="prev icon-chevron-sign-left"/>' )
				.append( '<div class="next icon-chevron-sign-right"/>' )

				.find( 'ul' )
					.cycle({
						speed:           800,
						timeout:         5000,
						delay:           -1000,
						pause:           true,
						prev:            '.case-studies.section-block .prev',
						next:            '.case-studies.section-block .next',
						pager:           false,
						containerResize: false,
						width:           '100%',
						fit:             1
					});
		});

		$('.past-events-slides').each(function(){
			if ($($(this).find('> *')).length > 1) {
				$(this).parent().append('<i class="past-events-prev"></i><i class="past-events-next"></i>');
			}

			$(this).cycle({
				speed:           800,
				timeout:         5000,
				delay:           -1000,
				pause:           true,
				prev:            '.past-events-prev',
				next:            '.past-events-next',
				pager:           false,
				containerResize: false,
				width:           '100%',
				fit:             1,
				random:          1
			});
		});

		$('.hero-slideshow').each(function(){
			$(this).cycle({
				speed:           800,
				timeout:         5000,
				delay:           -1000,
				pause:           true,
				pager:           false,
				containerResize: false,
				width:           '100%',
				fit:             1
			});
		});

	})();


	/*
    ==============================================================
		Toggle Slider
	==============================================================
	*/
	(function(){
		$('.slider').each(function(){

			// Kill empty DOM elements
			$(this).children(':empty').remove();

			// Add title class to first DOM element
			$(this).children(':first')
				.addClass('slider-title')

				// Add +/- symbols
				.append('<i class="icon-plus"></i><i class="icon-minus"></i>')

				// Slide toggle function
				.click(function() {
					$(this).parents('.slider').toggleClass('active');
					$(this).siblings('.slider-content').slideToggle();
					$(this).children('.icon-plus, .icon-minus').toggle();
				});

			// Wrap all DOM elements except title
			$(this).children().not('.slider-title').wrapAll('<div class="slider-content"></div>');

			$(this).find('.slider-content')
				// Pull top/bottom margins off slider content to avoid animation jump
				.each(function(){
					$(this).children(':first').css('margin-top', '0');
					$(this).children(':last').css('margin-bottom', '0');
				})
				// Hide all sliders
				.hide();

			// Hide all minus signs
			$(this).find('.icon-minus').hide();

		});
	})();



	/*
    ==============================================================
		Google Analytics Tracking
	==============================================================
	*/

	// Homepage slides
	$(".home-hero .hero-slide-link").click(function(event) {
		if (event.preventDefault) {
			event.preventDefault();
		}
		else {
			event.returnValue = false;
		}

		googleAnalyticsTrackEvent("Homepage Slide", "Click", $(this).children("h2").text().replace("\n", ""), null, $(this).attr("href"));
		console.log($(this).children("h2").text());
	});

	// View the event
	var eventObject = $(".event.type-event");

	if (eventObject.length > 0) {
		var eventTitle = eventObject.children("h1").text();

		googleAnalyticsTrackEvent("Event", "View Details", eventTitle);

		// Reserve your place click
		// Check if it is a form, or is a link to an external page
		if (eventObject.find("form .gform_body").length > 0) {
			$(document).on("gform_confirmation_loaded", function(event, formId) {
				googleAnalyticsTrackEvent("Event", "RSVP Click", eventTitle);
			});
		}
		else {
			eventObject.find("a.btn.cyan").click(function(event) {
				googleAnalyticsTrackEvent("Event", "RSVP Click", eventTitle);
			});
		}
	}

	// Resources (Articles and Videos)

	function resourceElementClick(resourceAnchorJQueryObject, categoryPrefix) {
		var resourceElement = resourceAnchorJQueryObject.closest(".resource,.case-study");

		var resourceType = null;

		if (resourceElement.hasClass("videos")) {
			resourceType = "Video";
		}
		else if (resourceElement.hasClass("articles")) {
			resourceType = "Resource";
		}
		else if (resourceElement.hasClass("case-study")) {
			resourceType = "Case Study";
		}
		else if (resourceElement.hasClass("post")) {
			resourceType = "Article";
		}
		else {
			resourceType = "Undefined Type";
		}

		if (resourceType != "Video") {
			if (event.preventDefault) {
				event.preventDefault();
			}
			else {
				event.returnValue = false;
			}
		}

		if ((categoryPrefix == "undefined") || (categoryPrefix == null)) {
			categoryPrefix = "Resources";
		}

		var eventLabel = null;

		if (resourceType == "Case Study") {
			var infoObject = resourceElement.children(".info.col");
			eventLabel = infoObject.children(".author").text() + " - " + infoObject.children(".position").text();
		}
		else {
			eventLabel = resourceAnchorJQueryObject.find(".entry-content .title").text();
		}

		googleAnalyticsTrackEvent(categoryPrefix + " - " + resourceType, "Click", eventLabel, null, (resourceType != "Video" ? resourceAnchorJQueryObject.attr("href") : null));
	}

	$(".resource.articles a, .resource.videos a, .case-study a.btn").click(function(event) {
		resourceElementClick($(this));
	});

	// SMSF Landing Page Specific Content

	if ($("body.page-template-template-smsf-event-php").length > 0) {
		// Reserve your place buttons
		$("a.btn.red:contains('Reserve Your Place')").click(function(event) {
			if (event.preventDefault) {
				event.preventDefault();
			}
			else {
				event.returnValue = false;
			}

			googleAnalyticsTrackEvent("SMSF Event Landing Page - Reserve Your Place", "Click", $(this).text().replace("Reserve Your Place –", ""), null, $(this).attr("href"));
		});

		$("li.resource.videos a,li.post a").click(function(event) {
			resourceElementClick($(this), "SMSF Event Landing Page Resource");
		});
	}

	// Podcasts
	$('.powerpress_player button').one('click', function(event) {
		// We just need to check the first time that someone requests to play the podcast
		if ($(this).attr('aria-label') == 'Play') {
			var category = 'Podcast';

			var action = 'Play';

			var label = null;

			var podcastItemContainer = $(this).closest('.resource.podcasts,.resource-podcasts');

			if (podcastItemContainer.hasClass('entry-callout')) {
				// It's a podcasts list item
				label = podcastItemContainer.find('.title').text();
			}
			else {
				// It's a podcast item page
				label = $(podcastItemContainer.find('h1')[0]).text();
			}

			googleAnalyticsTrackEvent(category, action, label);
		}
	});

	// Subscribe via iTunes button
	$('.btn--podcast-subscription').click(function(event) {
		var category = 'Podcast Subscription';

		var action = 'Click';

		var label = 'iTunes';

		googleAnalyticsTrackEvent(category, action, label);
	});

	/*
	============================================================
		About Us page lightbox
	============================================================
	*/

	var modorasHistory = new DbbHistoryState(false);

	var modorasIsReplaceState = false;

	var executiveMemberHref = null;

	if (/^(?:(?:[a-zA-Z0-9\-\_]+)|(?:\??.*?#))/.test(window.location.href.replace(window.appConfig.currentPageUrl, ''))) {
		modorasIsReplaceState = true;

		var executiveMemberHref = window.location.href;

		if (/^\#\!.*$/.test(window.location.hash)) {
			executiveMemberHref = window.location.hash.replace(/^\#\!(.*)$/, '$1');
		}
	}

	$('.executive-team__link').fancybox({
		type: 'inline',
		scrolling: 'visible',
		padding: 0,
		fitToView: false,
		margin: 0,
		autoPlay: false,
		maxWidth: '100%',
		wrapCSS: 'executive-team-modal',
		helpers: {
			overlay: {
				locked: true
			}
		},
		afterLoad: function (upcoming) {
			$('.member-file__video').show();
			$('.member-file__youtube-video').remove();

			var path = window.appConfig.currentPageUrl + upcoming.content.attr('id').replace('#', '') + '/';

			var state  = {
				content: upcoming.content.html(),
				path: path
			};

			if (modorasIsReplaceState) {
				modorasIsReplaceState = false;

				modorasHistory.replace(state, path, path);
			}
			else {
				modorasHistory.push(state, path, path);
			}

			upcoming.content.find('.member-file__video').one('click', function (event) {
				event.preventDefault();
				$(this).hide();
				$('<iframe class="member-file__youtube-video" frameborder="0" width="100%" height="100%"></iframe>')
					.attr('src', '//www.youtube.com/embed/' + $(this).data('video-id') + '?showinfo=0&rel=0&title=0&autohide=1&wmode=transparent&hd=1&autoplay=1&modestbranding=1')
					.insertAfter($(this));
			});
		},
		beforeClose: function () {
			$('.member-file__video').show();
			$('.member-file__youtube-video').remove();

			var state = {
				content: null,
				path: window.appConfig.currentPageUrl
			};

			modorasHistory.push(state, window.appConfig.currentPageUrl, window.appConfig.currentPageUrl);
		}
	});

	if (executiveMemberHref!== null) {
		$('.executive-team__link[href="' + executiveMemberHref + '"]').trigger('click');
	}


	$(window).on('onDbbHistoryStatePop', function() {
		if (modorasHistory.getState() === null || $.isEmptyObject(modorasHistory.getState())) {
			// No state
			return;
		}

		$('.executive-team__link[href="' + modorasHistory.getState().path + '"]').trigger('click');
	});

	/*
    ==============================================================
		Price Matrix 
	==============================================================
	*/

	var priceMatrixFormContainer = $('#priceMatrixFormContainer');
	
	var priceMatrixSelectedService = null;
	
	if (priceMatrixFormContainer.length > 0) {
		$('html').addClass('price-matrix__modal--lock-fix');
	}
	
	$('.price-matrix__item--button').click(function() {
		var serviceName = $(this).data('service-name');
		
		priceMatrixFormContainer
			.find('.gfield.gform_hidden > input[type="hidden"].gform_hidden').val(serviceName);
	});
	
	$('.price-matrix__item--button').fancybox({
		type: 'inline',
		scrolling: 'visible',
		padding: 0,
		fitToView: false,
		margin: 0,
		autoPlay: false,
		maxWidth: '100%',
		wrapCSS: 'price-matrix__modal',
		helpers: {
			overlay: {
				locked: true
			}
		},
		afterClose: function () {
			// We need to reload the form always to clean error messages
			priceMatrixFormContainer.html(
				'<p>Please fill out the following details:</p>' +
				modorasPriceMatrixFormHtml
			);
		}
	});
	
	/*
    ==============================================================
		Videos (add ids to each video)
	==============================================================
	*/

});

function setElementUniqueIdFromString(domObject, baseTitle) {
	var nextIndex = 0;

	var uniqueTitle = baseTitle;

	while ($("#" + uniqueTitle).length > 0) {
		uniqueTitle = baseTitle + "-" + ++nextIndex;
	}

	$(domObject).attr("id", uniqueTitle);

	return uniqueTitle;
}

function googleAnalyticsTrackEvent(category, action, label, value, urlAfterDelay) {
	if ((value != "undefined") && (value != null)) {
		ga("send", "event", category, action, label, value);
	}
	else {
		ga("send", "event", category, action, label);
	}

	if ((urlAfterDelay != "undefined") && (urlAfterDelay != null)) {
		setTimeout('document.location = "' + urlAfterDelay + '"', 100);
	}
}
