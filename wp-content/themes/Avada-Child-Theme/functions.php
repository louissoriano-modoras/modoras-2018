<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );




function modoras_is_client_area_login() {
	$websiteBaseUrl = rtrim(get_bloginfo('url'), '/') . '/';
	
	$basePath = preg_replace('/^(?:(?:(?:(?:http)|(?:https))\:)\/\/)(.*?)\/(.*)$/', '$2', $websiteBaseUrl);
	
	$basePath = trim($basePath, '/') . '/';
	
	if (substr($basePath, 0, 1) != '/') {
		$basePath = '/' . $basePath;
	}
	
	if (preg_match('/^' . preg_quote($basePath, '/') .  'client-area-login(?:(?:\/?)|(?:\/.*))$/', $_SERVER['REQUEST_URI'])) {
		return true;
	}
	
	return false;
}





function tribe_custom_theme_text ( $translation, $text, $domain ) {
 
	// Put your custom text here in a key => value pair
	// Example: 'Text you want to change' => 'This is what it will be changed to'
	// The text you want to change is the key, and it is case-sensitive
	// The text you want to change it to is the value
	// You can freely add or remove key => values, but make sure to separate them with a comma
	// This example changes the label "Venue" to "Location", and "Related Events" to "Similar Events"
	$custom_text = array(
		'Organizer' => 'Contact',
		
	);
 
	// If this text domain starts with "tribe-", "the-events-", or "event-" and we have replacement text
    	if( (strpos($domain, 'tribe-') === 0 || strpos($domain, 'the-events-') === 0 || strpos($domain, 'event-') === 0) && array_key_exists($translation, $custom_text) ) {
		$translation = $custom_text[$translation];
	}
    return $translation;
}
add_filter('gettext', 'tribe_custom_theme_text', 20, 3);



// modoras 
if ( is_user_logged_in() ) {
    add_filter('body_class','add_role_to_body');
    add_filter('admin_body_class','add_role_to_body');
}
function add_role_to_body($classes) {
    $current_user = new WP_User(get_current_user_id());
    $user_role = $current_user->ID;
    if (is_admin()) {
        $classes .= 'role-'. $user_role;
    } else {
        $classes[] = 'role-'. $user_role;
    }
    return $classes;
}

// modoras 
add_filter( 'avada_blog_read_more_excerpt', 'my_read_more_symbol' );
function my_read_more_symbol( $read_more ) {
	$read_more = "";
	
	return $read_more;
}


// modoras
add_action('admin_head', 'admin_custom_css');

function admin_custom_css() {
  echo '<style>
    .toplevel_page_ai1wm_export,
    .toplevel_page_edit-post_type-acf-field-group,
    .menu-posts-avada_portfolio,
    #toplevel_page_edit-post_type-acf-field-group,
    .toplevel_page_insert-php-code-snippet-manage,
    .menu-icon-themefusion_elastic,
    .toplevel_page_yotuwp,
    .menu-icon-avada_portfolio,
    .toplevel_page_fusion-white-label-branding-admin,
    .toplevel_page_fusion-builder-options,
    #menu-posts-slide,
    .toplevel_page_avada {
      display: none;
    }
    .role-1 .toplevel_page_ai1wm_export,
    .role-1.toplevel_page_ai1wm_export,
    .role-1 .toplevel_page_edit-post_type-acf-field-group,
    .role-1.toplevel_page_edit-post_type-acf-field-group,
    .role-1 .menu-posts-avada_portfolio,
    .role-1.menu-posts-avada_portfolio,
    .role-1 #toplevel_page_edit-post_type-acf-field-group,
    .role-1#toplevel_page_edit-post_type-acf-field-group,
    .role-1 .toplevel_page_insert-php-code-snippet-manage,
    .role-1.toplevel_page_insert-php-code-snippet-manage,
    .role-1 .menu-icon-themefusion_elastic,
    .role-1.menu-icon-themefusion_elastic,
    .role-1 .toplevel_page_yotuwp,
    .role-1 .menu-icon-avada_portfolio,
    .role-1.menu-icon-avada_portfolio,
    .role-1 .toplevel_page_fusion-white-label-branding-admin,
    .role-1.toplevel_page_fusion-white-label-branding-admin,
    .role-1 .toplevel_page_fusion-builder-options,
    .role-1.toplevel_page_fusion-builder-options,
    .role-1 #menu-posts-slide,
    .role-1#menu-posts-slide,
    .role-1 .toplevel_page_avada,
    .role-1.toplevel_page_avada {
      display: block !important;
    } 
  </style>';
}

// Modoras -- DO NOT REMOVE IT
function modoras_php56_allow_not_valid_ca_certificates($phpMailer) {
    $phpMailer->SMTPOptions = array(
            'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
            )
    );
}
add_action('phpmailer_init', 'modoras_php56_allow_not_valid_ca_certificates', 10, 1);